<?php
/* ------------------------------------------------------------------------- *
 *	Page heading template
/* ------------------------------------------------------------------------- */
?>

<header id="top-header-<?php the_ID(); ?>" class="grid-wrap page-heading heading-full-width clearfix"<?php businessx_sp_parallax(); ?>>
	<div class="grid-overlay"></div>
    <?php do_action( 'businessx_phfw__inner_top' ); ?>
	<div class="sec-hs-elements ta-center">
    	<?php do_action( 'businessx_phfw__heading_top' ); ?>
    		
            <?php if ( is_category( 'Bloggar' ) ) : ?>
                <h1 class="hs-primary-large">Bloggar</h1>

            <?php elseif ( is_category( 'Nyheter' ) ): ?>
                <h1 class="hs-primary-large">Nyheter</h1>

            <?php elseif ( is_category( 'Elsa' ) ): ?>
                <h1 class="hs-primary-large">Elsas blogg</h1>

            <?php elseif ( is_category( 'Alexandra' ) ): ?>
                <h1 class="hs-primary-large">Alexandas blogg</h1>

            <?php elseif ( is_category( 'Emma' ) ): ?>
                <h1 class="hs-primary-large">Emmas blogg</h1>

            <?php elseif ( is_category( 'Sofija' ) ): ?>
                <h1 class="hs-primary-large">Sofijas blogg</h1>

            <?php else: ?>
                <?php the_title( '<h1 class="hs-primary-large">', '</h1>' ); ?>
            <?php endif; ?>

        <?php do_action( 'businessx_phfw__heading_bottom' ); ?>
    </div>
    <?php do_action( 'businessx_phfw__inner_bottom' ); ?>
</header>

<?php do_action( 'businessx_phfw__after' ); ?>
