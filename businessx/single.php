<?php
/* ------------------------------------------------------------------------- *
 * Single template
/* ------------------------------------------------------------------------- */

/**
 * Filtered CSS classes
 * ------
 * section: grid-wrap
 * div: grid-container grid-1 padding-small clearfix
 * main: grid-col grid-posts-col site-single clearfix
 * ------
 */

// Header and Footer templates
$businessx_header_tmpl = apply_filters( 'businessx_header___tmpl', '' );
$businessx_footer_tmpl = apply_filters( 'businessx_footer___tmpl', '' );

// Header
get_header( $businessx_header_tmpl );
?>

<section role="main" id="content" class="<?php businessx_occ( 'businessx_single___section_classes' ); ?> plus-margin">
	<?php do_action( 'businessx_single__inner_sec_top' ); ?>

	<div class="<?php businessx_occ( 'businessx_single___container_classes' ); ?>">

		<?php do_action( 'businessx_single__inner_before' ); ?>

		<main id="main" class="<?php businessx_occ( 'businessx_single___main_classes' ); ?>" role="main">

			<?php if (have_posts()) :
				while (have_posts()) : the_post();?>
					<h2 class="post-head">
						<u class="blue-underline"><?php the_title(); ?></u>
					</h2>
					<article class="single-post">
						<?php if ( has_post_thumbnail() ) { ?>
							<?php the_post_thumbnail( 'large-thumbnail' ); ?>
							<div class="post-content">
								<?php the_content(); ?>
							</div>
						<?php } else { ?>
							<div class="post-content">
								<?php the_content(); ?>
							</div>
						<?php } ?>
					</article>
					
				<?php endwhile; ?>

				<div class="post-details">
					<ul class="article-info">
						<span class="details-text">Publicerad:</span>
						<li><?php the_date(); ?></li>
					</ul>
				</div>
			<?php else :
				echo '<p>No content found</p>';
			endif; ?>



		</main>

		<?php get_sidebar( apply_filters( 'businessx_sidebar___single', 'single' ) ); ?>

		<?php do_action( 'businessx_single__inner_after' ); ?>

	</div>

    <?php do_action( 'businessx_single__inner_sec_bottom' ); ?>
</section>

<?php
// Footer
get_footer( $businessx_footer_tmpl ); ?>
